Vue.use(VeeValidate);
var contactForm = new Vue({
    el: '#app',
    data: {
        email: null,
        phone: null,
        name: null,
        surrname: null,
        message: null,
        errorss: [],
    },
    methods: {
        checkForm: function (e) {

            if (this.email && this.phone && this.name && this.surrname && this.message) {
                return true;
            }

            this.errorss = [];

            if (!this.email) {
                this.errorss.push('Email required.');
            }else if (!this.validEmail(this.email)){
                this.errorss.push('Valid email required');
            }

            if (!this.phone) {
                this.errorss.push('Phone required.');
            }            
            if (!this.name) {
                this.errorss.push('Name required.');
            }            
            if (!this.surrname) {
                this.errorss.push('Surrname required.');
            }            
            if (!this.message) {
                this.errorss.push('Message required.');
            }            

            e.preventDefault();
        },
        validEmail: function (email) {
            var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return reg.test(email);
        },
        submitForm : () => {
            alert('Form submit');
        }
    }
})